@extends('partials.master')
@section('title', 'Update-Golongan')
@section('content')
@section('content-title', 'Update Golongan')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <h5><a class="fa fa-arrow-left mb-3" href="{{route('golongan')}}" style="color: dimgray"></a></h5>
                <form class="form-horizontal" action="{{route('tambah_golongan')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="tingkat" class="col-sm-2 col-form-label">Tingkat</label>
                        <div class="col-sm-10"> 
                            <input type="number" class="form-control @error('tingkat') is-invalid @enderror" name="tingkat" placeholder="Tingkat">
                            {!! $errors->first('tingkat','<span class="invalid-feedback">Tingkat harus diisi</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="golongan" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('golongan') is-invalid @enderror" name="golongan" placeholder="Golongan"></input>
                            {!! $errors->first('golongan','<span class="invalid-feedback">Golongan harus diisi</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pangkat" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('pangkat') is-invalid @enderror" name="pangkat" placeholder="Pangkat"></input>
                            {!! $errors->first('pangkat','<span class="invalid-feedback">Pangkat harus diisi</span>') !!}
                        </div>
                    </div>
                    
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
@endsection('content')