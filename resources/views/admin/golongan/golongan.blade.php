@extends('partials.master')
@section('title', 'Golongan')
@section('content')
@section('content-title', 'Golongan')
<div class="row">
    <div class="col-12 pb-3">
        <a class="btn btn-success" href="{{route('tambah_golongan')}}">Tambah Golongan</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Tingkat</th>
                            <th>Golongan</th>
                            <th>Pangkat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($gol as $g)
                        <tr>
                            <td>{{ $g->tingkat  }}</td>
                            <td>{{ $g->golongan }}</td>
                            <td>{{ $g->pangkat }}</td>

                            <td align="center">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a class="nav-icon fas fa-edit" style="color:dimgray" href="{{route('update_golongan', $g)}}"></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <!-- Tombol untuk menampilkan modal-->
                                        <a class="nav-icon fas fa-trash" data-toggle="modal" data-target="#deleteModal{{ $g->id }}" style="color:dimgray"></a>

                                        <!-- Modal -->
                                        <div id="deleteModal{{ $g->id }}" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- konten modal-->
                                                <div class="modal-content">
                                                    <!-- heading modal -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Apakah anda yakin ingin menghapus?</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body" align="right">
                                                        <form action="{{route('hapus_golongan', $g)}}" method="post">
                                                            @csrf
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
<!-- /.card -->

@endsection('content')