@extends('partials.master')
@section('title', 'Update-Golongan dan Pangkat')
@section('content')
@section('content-title', 'Update Golongan dan Pangkat')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <h5><a class="fa fa-arrow-left mb-3" href="{{route('golongan')}}" style="color: dimgray"></a></h5>
                <form class="form-horizontal" action="{{route('update_golongan', $gol)}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="tingkat" class="col-sm-2 col-form-label">Tingkat</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control @error('tingkat') is-invalid @enderror" name="tingkat" placeholder="Tingkat" value="{{$gol->tingkat}}">
                            {!! $errors->first('tingkat','<span class="invalid-feedback">Tingkat harus diisi</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="golongan" class="col-sm-2 col-form-label">Golongan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('golongan') is-invalid @enderror" name="golongan" placeholder="Golongan" value="{{$gol->golongan}}"></input>
                            {!! $errors->first('golongan','<span class="invalid-feedback">Golongan harus diisi</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pangkat" class="col-sm-2 col-form-label">Pangkat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('pangkat') is-invalid @enderror" name="pangkat" placeholder="Pangkat" value="{{$gol->pangkat}}"></input>
                            {!! $errors->first('pangkat','<span class="invalid-feedback">Pangkat harus diisi</span>') !!}
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
    @endsection('content')