@extends('partials.master')
@section('title', 'SPT')
@section('content')
@section('content-title', 'SPT')
<div class="row">
    <div class="col-12 pb-3">
        <a class="btn btn-success" href="{{route('tambah_spt')}}" style="margin-right: 10px">Tambah SPT</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table table id="table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No. Surat</th>
                            <th>Tanggal</th>
                            <th>Pegawai</th>
                            <th>Tujuan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($spt as $s)
                        <tr>
                            <td>
                                <?php if ($s->nomor_surat) { ?>
                                    094/{{$s->nomor_surat}}/114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                <?php } else { ?>
                                    094/ &nbsp; &nbsp; &nbsp; /114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                <?php } ?>
                            </td>
                            <td>{{ Date::parse($s->tgl_keluar)->format('F Y')}}</td>
                            <td>
                                @foreach($s->pegawai as $p)
                                {{$p->nama}} <br>
                                @endforeach
                            </td>
                            <td>{{Str::limit($s->tujuan, 100)}}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <!-- Trigger the modal with a button -->
                                        <a class="nav-icon fas fa-eye" style="color:dimgray" data-toggle="modal" data-target="#myModal{{ $s->id }}"></a>

                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal{{$s->id}}" role="dialog">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Surat Perintah Tugas - {{$s->nomor_surat}}</h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                    </div>
                                                    <div class="modal-body">

                                                        <table width="100%" class="table table-borderless ">
                                                            <tr>
                                                                <td align="center">
                                                                    <b><u>SURAT PERINTAH TUGAS</u></b><br>
                                                                    Nomor : <?php if ($s->nomor_surat) { ?>
                                                                        094/{{$s->nomor_surat}}/114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                                                    <?php } else { ?>
                                                                        094/ &nbsp; &nbsp; &nbsp; /114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table width="100%" class="table table-borderless ">
                                                            <tr>
                                                                <td style="vertical-align: top;">
                                                                    <b>Dasar</b>
                                                                </td>
                                                                <td style="vertical-align: top;"><b>:</b></td>
                                                                <td>
                                                                    <table class="table table-borderless ">
                                                                        @foreach($dasar as $d)
                                                                        <tr>
                                                                            <td style="vertical-align: top;">
                                                                                {{$d->nomor}}.
                                                                            </td>
                                                                            <td align="justify">
                                                                                {{$d->deskripsi}}
                                                                            </td>
                                                                        </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>


                                                        <table width="100%" class="table table-borderless ">
                                                            <tr>
                                                                <td align="center">
                                                                    <b><u>M E M E R I N T A H K A N</u></b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="table table-borderless ">
                                                            <tr>
                                                                <td class="vertical-align: top;">
                                                                    <b>Kepada</b>
                                                                </td>
                                                                <td style="vertical-align: top;"><b>:</b></td>
                                                                <td style="width: 89%">
                                                                    <table class="table table-borderless ">
                                                                        <?php $i = 1 ?>
                                                                        @foreach($s->pegawai as $p)
                                                                        <tr>
                                                                            <td style="width: 4%">{{$i++}}. </td>
                                                                            <td style="width: 21%">Nama</td>
                                                                            <td style="width: 5%"> : </td>
                                                                            <td style="width: 70%">{{$p->nama}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>NIP</td>
                                                                            <td> : </td>
                                                                            <td>{{$p->nip}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>Pangkat/Gol</td>
                                                                            <td> : </td>
                                                                            <td>
                                                                                <?php
                                                                                if ($p->golongan_pangkat->tingkat == 0) {
                                                                                    echo "-";
                                                                                } else { ?>
                                                                                    {{$p->golongan_pangkat->pangkat}} ({{$p->golongan_pangkat->golongan}})
                                                                                <?php } ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>Jabatan</td>
                                                                            <td> : </td>
                                                                            <td>{{$p->jabatan}}</td>
                                                                        </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="table table-borderless ">
                                                            <tr>
                                                                <td width="10%" style="vertical-align: top;">
                                                                    <b>Untuk </b>
                                                                </td>
                                                                <td width="5%" style="vertical-align: top;"><b>:</b></td>
                                                                <td width="75%" align="justify">
                                                                    {{$s->tujuan}}
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">


                                                        <form action="{{route('hapus_spt', $s)}}" method="post">
                                                            @csrf
                                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                                        </form>
                                                        <form action="{{route('update_spt', $s)}}" method="get">

                                                            <button type="submit" class="btn btn-secondary"> Edit </button>
                                                        </form>

                                                        <form action="{{route('cetak_spt', $s)}}" method="post" target="_blank">
                                                            @csrf
                                                            <button type="submit" class="btn btn-primary">Cetak</button>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="nav-icon fas fa-archive" style="color:dimgray" href="{{route('arsip_spt', $s)}}"></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

</div>

<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }

    $(function() {
        $("#table").DataTable({
            "ordering": false
        });
    });
</script>

@endsection('content')

