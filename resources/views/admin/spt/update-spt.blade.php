@extends('partials.master')
@section('title', 'SPT')
@section('content')
@section('content-title', 'SPT')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <h5><a class="fa fa-arrow-left mb-3" href="{{route('spt')}}" style="color: dimgray"></a></h5>
                <h5><b>
                        <div align="center" class="col-1"></div>
                        Dasar
                    </b>
                </h5>
                @foreach($dasar as $d)
                <div class="row">
                    <div align="center" class="col-1 ">
                        <h>{{$d->nomor}}. </h>
                    </div>
                    <div class="col-11 float-left">
                        <h>{{$d->deskripsi}}</h>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <form class="form-horizontal" action="{{route('update_spt', $spt)}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="tglPelaksanaan" class="col-sm-2 col-forn-label">Tanggal keluar</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm-yyyy" name="tglKeluar" value="{{tommyyyy($spt->tgl_keluar)}}"  data-mask>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pegawaiSPT" class="col-sm-2 col-form-label">Pegawai</label>
                        <div class="col-sm-10">
                            <!-- <input type="text" class="form-control" id="bidangPegawai" placeholder="Bidang"> -->
                            <select class="form-control select2bs4 @error('pegawaiSPT') is-invalid @enderror" name="pegawaiSPT[]" multiple="multiple" data-placeholder="Pilih pegawai" style="width: 100%;" >
                                @foreach($pegawai as $p)
                                <option value="{{$p->id}}"  @foreach($spt->pegawai as $choosen) {{$p->id == $choosen->id ? 'selected' : '' }} @endforeach>{{$p->nama}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('pegawaiSPT','<span class="invalid-feedback">Pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nomorSPT" class="col-sm-2 col-form-label">Nomor Surat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('nomorSPT') is-invalid @enderror" name="nomorSPT" placeholder="Nomor surat" data-inputmask="'mask': '9999'" value="{{$spt->nomor_surat}}">
                            {!! $errors->first('nomorSPT','<span class="invalid-feedback">Nomor SPT harus diisi!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tujuanSPT" class="col-sm-2 col-form-label">Tujuan</label>
                        <div class="col-sm-10">
                            <textarea class="form-control @error('tujuanSPT') is-invalid @enderror" name="tujuanSPT" placeholder="Tujuan" rows="4" value="{{old('tujuanSPT')}}">{{$spt->tujuan}}</textarea>
                            {!! $errors->first('tujuanSPT','<span class="invalid-feedback">Tujuan SPT harus diisi!</span>') !!}
                        </div>
                    </div>
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
    <script>
        $(function() {
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', {
                'placeholder': 'dd/mm/yyyy'
            })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', {
                'placeholder': 'mm/dd/yyyy'
            })
        })

        $(":input").inputmask();
    </script>
    @endsection('content')