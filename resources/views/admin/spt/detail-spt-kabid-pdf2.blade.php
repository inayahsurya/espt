<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pdf.css') }}">
</head>

<body>
    <table width="100%">
        <tr><td align="center" style="width: 15%"><img src="{{asset('assets/img/jatim-bnw.jpg')}}" width="60px"></td>
            <td align="center" style="width: 85%">
                PEMERINTAH PROVINSI JAWA TIMUR <br>
                <b class="diskominfo">DINAS KOMUNIKASI DAN INFORMATIKA</b><br>
                JL. A. Yani 242-244 Surabaya. Telp. (031) 8294608, Fax. (031) 8294517 <br>
                Website : kominfo.jatimprov.go.id <br>
                E-mail : kominfo@jatimprov.go.id <br>
                <u><b>SURABAYA 60235</b></u>
            </td>
        </tr>
    </table>
    <br><br><br>
    <table width="100%">
        <tr>
            <td align="center">
                <b><u>SURAT PERINTAH TUGAS</u></b><br>
                Nomor : <?php if ($spt->nomor_surat) { ?>
                    094/{{$spt->nomor_surat}}/114.{{$spt->id_bidang}}/{{ Date::parse($spt->tgl_keluar)->format('Y')}}
                <?php } else { ?>
                    094/ &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; /114.{{$spt->id_bidang}}/{{ Date::parse($spt->tgl_keluar)->format('Y')}}
                <?php } ?>
            </td>
        </tr>
    </table>
    <br><br>
    <table width="100%">
        <tr>
            <td style="vertical-align: top;" style="width: 13%"><b>Dasar</b></td>
            <td style="vertical-align: top;" style="width: 2%"><b>:</b></td>
            <td style="width: 85%">
                <table>
                    @foreach($dasar as $d)
                    <tr>
                        <td style="vertical-align: top; width: 4%">{{$d->nomor}}.</td>
                        <td style="text-align: justify; width: 96%">{{$d->deskripsi}}</td>
                    </tr>
                    @endforeach
                </table>
            </td>
        </tr>
    </table>
    <br><br>
    <table width="100%">
        <tr>
            <td align="center"><b><u>M E M E R I N T A H K A N</u></b></td>
        </tr>
    </table>
    <br><br>
    <table>
        <tr>
            <td style="vertical-align: top;" style="width: 13%"><b>Kepada</b></td>
            <td style="vertical-align: top;" style="width: 2%"><b>:</b></td>
            <td style="width: 85%"><i>Terlampir pada lampiran 1</i></td>
        </tr>
    </table>
    <br><br>
    <table>
        <tr>
        <td style="vertical-align: top;" style="width: 13%"><b>Untuk</b></td>
            <td style="vertical-align: top;" style="width: 2%"><b>:</b></td>
            <td style="width: 85%">
                <table>
                    <tr>
                        <td style="vertical-align: top; text-align: justify">{{$spt->tujuan}}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br><br><br><br><br>
    <table style="width: 100%">
        <tr>
            <td>
                <table width="140%">
                    <tr>
                        <td align="center">
                            Surabaya, <h> &nbsp; &nbsp; &nbsp; &nbsp; </h>
                            {{ Date::parse($spt->tgl_keluar)->format('F Y')}}
                            <br>
                            KEPALA DINAS KOMUNIKASI DAN INFORMATIKA <br>
                            PROVINSI JAWA TIMUR <br>
                        </td>
                    </tr>
                </table>
                <br><br><br><br><br>
                <?php $nip = printNip($ttd->nip, $ttd->status); ?>
                <table width="140%">
                    <tr>
                        <td align="center"><b><u>{{$ttd->nama}}</u></b><br>{{$ttd->golongan_pangkat->pangkat}}<br>NIP. {{$nip}}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br pagebreak="true" />
    <table width="100%">
        <tr>
            <td align="center" style="width: 15%"><img src="{{asset('assets/img/jatim-bnw.jpg')}}" width="67px"></td>
            <td align="center" style="width: 85%">
                PEMERINTAH PROVINSI JAWA TIMUR <br>
                <b class="diskominfo">DINAS KOMUNIKASI DAN INFORMATIKA</b><br>
                JL. A. Yani 242-244 Surabaya. Telp. (031) 8294608, Fax. (031) 8294517 <br>
                Website : kominfo.jatimprov.go.id <br>
                E-mail : kominfo@jatimprov.go.id <br>
                <u><b>SURABAYA 60235</b></u>
            </td>
        </tr>
    </table>
    <br><br><i> Lampiran 1</i><br><br>
    <table border="1" style="padding: 5px">
        <tr align="center">
            <td style="width: 6%">No.</td>
            <td style="width: 17%">Nama</td>
            <td style="width: 27%">NIP</td>
            <td style="width: 14%">Pangkat/Gol</td>
            <td style="width: 36%">Jabatan</td>
        </tr>
        <?php $i = 1 ?>
        @foreach($peg as $p)
        <?php
        $nip = printNip($p->nip, $p->status);
        ?>
        <tr>
            <td style="text-align: center">{{$i++}}.</td>
            <td>{{$p->nama}}</td>
            <td>{{$nip}}</td>
            <td><?php if ($p->golongan_pangkat->tingkat == 0) { echo "-"; } else {?>{{$p->golongan_pangkat->pangkat}} ({{$p->golongan_pangkat->golongan}}) <?php } ?> </td>
            <td>{{$p->jabatan}}</td>
        </tr>
        @endforeach
    </table>
</body>

<style type="text/css">
    body {
        font-size: 11pt;
        font-family: Arial, Helvetica, sans-serif;
    }

    .diskominfo {
        font-size: 14pt;
    }
</style>

</html>