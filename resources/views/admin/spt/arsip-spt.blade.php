@extends('partials.master')
@section('title', 'SPT')
@section('content')
@section('content-title', 'Arsip SPT')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            {{ $error }} <br />
            @endforeach
        </div>
        @endif
        <div class="card">
            <div class="card-body">
                <form class="form-horizontal" action="{{route('arsip_spt', $spt)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputFile">Upload arsip SPT</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile" name="arsipSpt">
                                <label class="custom-file-label" for="exampleInputFile/">Pilih file</label>
                            </div>
                            <div class="input-group-append">
                                <button class="input-group-text" type="submit">Upload</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php if ($imgSpt->count() != 0) { ?>
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table ">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>File</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                            @foreach($imgSpt as $g)
                            <tr>
                                <td>{{$i++}}</td>
                                <td><img width="760px" src="{{asset('/data_file/'.$spt->id.'/'.$g->file)}}" alt=""></td>
                                <td>
                                    <form action="{{route('hapus_arsip_spt', $g)}}">
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } else { ?>
            <div align="center">
                Belum ada arsip
            </div>
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        bsCustomFileInput.init();
    });
</script>
@endsection('content')