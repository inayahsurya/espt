@extends('partials.master')
@section('title', 'Tambah-Bidang')
@section('content')
@section('content-title', 'Tambah Bidang')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <h5><a class="fa fa-arrow-left mb-3" href="{{route('bidang')}}" style="color: dimgray"></a></h5>

                <form class="form-horizontal" action="{{route('tambah_bidang')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="nomorBidang" class="col-sm-2 col-form-label">Nomor bidang</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('nomorBidang') is-invalid @enderror" name="nomorBidang" placeholder="Nomor bidang" value="{{old ('nomorBidang')}}"></input>
                            {!! $errors->first('nomorBidang','<span class="invalid-feedback">Nomor bidang harus diisi!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="namaBidang" class="col-sm-2 col-form-label">Nama bidang</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('namaBidang') is-invalid @enderror" name="namaBidang" placeholder="Nama bidang" value="{{old ('namaBidang')}}"></input>
                            {!! $errors->first('namaBidang','<span class="invalid-feedback">Nama bidang harus diisi!</span>') !!}
                        </div>
                    </div>
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
    @endsection('content')