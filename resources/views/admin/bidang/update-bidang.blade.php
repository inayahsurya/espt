@extends('partials.master')
@section('title', 'Update-Bidang')
@section('content')
@section('content-title', 'Update Bidang')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <h5><a class="fa fa-arrow-left mb-3" href="{{route('bidang')}}" style="color: dimgray"></a></h5>
                <form class="form-horizontal" action="{{route('update_bidang', $bidang)}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="nomorBidang" class="col-sm-2 col-form-label">Nomor</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control @error('nomorBidang') is-invalid @enderror" name="nomorBidang" placeholder="Nomor" value="{{$bidang->nomor}}">
                            {!! $errors->first('nomorBidang','<span class="invalid-feedback">Nomor harus diisi</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="namaBidang" class="col-sm-2 col-form-label">Nama bidang</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('namaBidang') is-invalid @enderror" name="namaBidang" placeholder="Deskripsi" value="{{$bidang->nama_bidang}}"></input>
                            {!! $errors->first('namaBidang','<span class="invalid-feedback">Nama bidang harus diisi</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pegawai" class="col-sm-2 col-form-label">Nama kepala bidang</label>
                        <div class="col-sm-10">
                            <select class="form-control select2bs4 @error('pegawai') is-invalid @enderror" name="pegawai" style="width: 100%;">
                                <option></option>
                                @foreach($peg as $p)
                                <option value="{{$p->id}}" {{$bidang->kepala_bidang ? $p->id == $bidang->kepala_bidang->pegawai->id ? 'selected' : '' : ''}}> {{$p->nama}} </option>
                                @endforeach
                            </select>
                            {!! $errors->first('pegawai','<span class="invalid-feedback">Nama kepala bidang tidak boleh kosong!</span>') !!}
                        </div>
                    </div>

                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    </script>
    @endsection('content')