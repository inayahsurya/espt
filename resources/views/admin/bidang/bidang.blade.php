@extends('partials.master')
@section('title', 'Bidang')
@section('content')
@section('content-title', 'Bidang')
<div class="row">
    <div class="col-12 pb-3">
        <a class="btn btn-success" href="{{route('tambah_bidang')}}">Tambah Bidang</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">

            <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Bidang</th>
                            <th>Nama Kepala Bidang</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                        @foreach($bidang as $b)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$b->nama_bidang}}</td>
                            <td>{{$b->kepala_bidang ? $b->kepala_bidang->pegawai->nama : ''}}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a class="nav-icon fas fa-edit" style="color:dimgray" href="{{route('update_bidang', $b)}}"></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <!-- Tombol untuk menampilkan modal-->
                                        <a class="nav-icon fas fa-trash" data-toggle="modal" data-target="#deleteModal{{ $b->id }}" style="color:dimgray"></a>

                                        <!-- Modal -->
                                        <div id="deleteModal{{ $b->id }}" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- konten modal-->
                                                <div class="modal-content">
                                                    <!-- heading modal -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Apakah anda yakin ingin menghapus?</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body" align="right">
                                                        <form action="{{route('hapus_bidang', $b)}}" method="post">
                                                            @csrf
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- <li class="list-inline-item">
                                        <a class="nav-icon fas fa-trash" style="color:dimgray" href="{{route('hapus_dasar', $b)}}"></a>
                                    </li> -->
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

@endsection('content')