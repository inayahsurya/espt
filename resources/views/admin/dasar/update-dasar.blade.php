@extends('partials.master')
@section('title', 'Update-Dasar')
@section('content')
@section('content-title', 'Update Dasar')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <h5><a class="fa fa-arrow-left mb-3" href="{{route('dasar')}}" style="color: dimgray"></a></h5>
                <form class="form-horizontal" action="{{route('update_dasar', $dasar)}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="nomorDasar" class="col-sm-2 col-form-label">Nomor</label>
                        <div class="col-sm-10"> 
                            <input type="number" class="form-control @error('nomorDasar') is-invalid @enderror" name="nomorDasar" placeholder="Nomor" value="{{$dasar->nomor}}">
                            {!! $errors->first('nomorDasar','<span class="invalid-feedback">Nomor harus diisi</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deskripsiDasar" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control @error('deskripsiDasar') is-invalid @enderror" name="deskripsiDasar" placeholder="Deskripsi" rows="5">{{$dasar->deskripsi}}</textarea>
                            {!! $errors->first('deskripsiDasar','<span class="invalid-feedback">Deskripsi harus diisi</span>') !!}
                        </div>
                    </div>
                    
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
@endsection('content')