@extends('partials.master')
@section('title', 'Dasar')
@section('content')
@section('content-title', 'Dasar')
<div class="row">
    <div class="col-12 pb-3">
        <a class="btn btn-success" href="{{route('tambah_dasar')}}">Tambah Dasar</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Dasar</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dasars as $d)
                        <tr>
                            <td>{{$d->nomor}}</td>
                            <td>{{$d->deskripsi}}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a class="nav-icon fas fa-edit" style="color:dimgray" href="{{route('update_dasar', $d)}}"></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <!-- Tombol untuk menampilkan modal-->
                                        <a class="nav-icon fas fa-trash" data-toggle="modal" data-target="#myModal{{ $d->id }}" style="color:dimgray"></a>

                                        <!-- Modal -->
                                        <div id="myModal{{ $d->id }}" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- konten modal-->
                                                <div class="modal-content">
                                                    <!-- heading modal -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Apakah anda yakin ingin menghapus?</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body" align="right">
                                                        <form action="{{route('hapus_dasar', $d)}}" method="post">
                                                            @csrf
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- <li class="list-inline-item">
                                        <a class="nav-icon fas fa-trash" style="color:dimgray" href="{{route('hapus_dasar', $d)}}"></a>
                                    </li> -->
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>

@endsection('content')