@extends('partials.master')
@section('title', 'Tambah-Pegawai')
@section('content')
@section('content-title', 'Tambah Pegawai')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <a class="fa fa-arrow-left mb-3" href="{{route('pegawai')}}" style="color: dimgray"></a>
                <form class="form-horizontal" action="{{route('tambah_pegawai')}}" method="post">
                    @csrf

                    <div class="form-group row">
                        <label for="namaPegawai" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('namaPegawai') is-invalid @enderror" name="namaPegawai" placeholder="Nama" value="{{old ('namaPegawai')}}">
                            {!! $errors->first('namaPegawai','<span class="invalid-feedback">Nama pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nipPegawai" class="col-sm-2 col-form-label">NIP</label>
                        <div class="col-sm-10">

                            <input type="number" class="form-control @error('nipPegawai') is-invalid @enderror" name="nipPegawai" id="nip" placeholder="NIP" value="{{old ('nipPegawai')}}">
                            {!! $errors->first('nipPegawai','<span class="invalid-feedback">NIP pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="statusPegawai" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4 @error('statusPegawai') is-invalid @enderror" name="statusPegawai" style="width: auto;" value="{{old ('statusPegawai')}}">
                                <option></option>
                                <option value="1" @if(old('statusPegawai') == 1) {{'selected'}} @endif>PNS</option>
                                <option value="2" @if(old('statusPegawai') == 2) {{'selected'}} @endif>PTT</option>
                            </select>
                            {!! $errors->first('statusPegawai','<span class="invalid-feedback">Status pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="golonganPegawai" class="col-sm-2 col-form-label">Pangkat/Golongan</label>
                        <div class="col-sm-10">
                            <!-- <input type="text" class="form-control" id="golonganPegawai" placeholder="Bidang"> -->
                            <select class="form-control select2bs4 @error('golonganPegawai') is-invalid @enderror" name="golonganPegawai" style="width: 100%;" value="{{old ('golonganPegawai')}}">
                                <option></option>
                                @foreach($golonganPangkat as $g)
                                <option value="{{$g->tingkat}}" @if(old('golonganPegawai') == $g->tingkat) {{'selected'}} @endif>{{$g->pangkat}} ({{$g->golongan}})</option>
                                @endforeach
                            </select>
                            {!! $errors->first('golonganPegawai','<span class="invalid-feedback">Golongan pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jabatanPegawai" class="col-sm-2 col-form-label">Jabatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('jabatanPegawai') is-invalid @enderror" name="jabatanPegawai" placeholder="Jabatan" value="{{old ('jabatanPegawai')}}">
                            {!! $errors->first('jabatanPegawai','<span class="invalid-feedback">Jabatan pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bidangPegawai" class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                            <!-- <input type="text" class="form-control" id="bidangPegawai" placeholder="Bidang"> -->
                            <select class="form-control select2bs4 @error('bidangPegawai') is-invalid @enderror" name="bidangPegawai" style="width: 100%;" value="{{old ('bidangPegawai')}}">
                                <option></option>
                                @foreach($bidangs as $b)
                                <option value="{{$b->id}}" @if(old('bidangPegawai') == $b->id) {{'selected'}} @endif>{{$b->nama_bidang}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('bidangPegawai','<span class="invalid-feedback">Bidang pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>

    <script>
        $(function() {
            $(":input").inputmask();
        })

    </script>

    @endsection('content')