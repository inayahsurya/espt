@extends('partials.master')
@section('title', 'Pegawai')
@section('content')
@section('content-title', 'Pegawai')
<div class="row">
    <div class="col-12 pb-3">
        <a class="btn btn-success" href="{{route('tambah_pegawai')}}">Tambah Pegawai</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>NIP</th>
                            <th>Pangkat/Gol</th>
                            <th>Jabatan</th>
                            <th>Bidang</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pegawais as $p)
                        <tr>
                            <td>{{ $p->nama }}</td>
                            <td>{{ $p->nip }}</td>
                            <td>{{ $p->golongan_pangkat->pangkat}} ({{ $p->golongan_pangkat->golongan}})</td>
                            <td>{{ $p->jabatan }}</td>
                            <td>{{ $p->bidang ? $p->bidang->nama_bidang : ''}}</td>
                            <td align="center">
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a class="nav-icon fas fa-edit" style="color:dimgray" href="{{route('update_pegawai', $p)}}"></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <!-- Tombol untuk menampilkan modal-->
                                        <a class="nav-icon fas fa-trash" data-toggle="modal" data-target="#myModal{{ $p->id }}" style="color:dimgray"></a>

                                        <!-- Modal -->
                                        <div id="myModal{{ $p->id }}" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- konten modal-->
                                                <div class="modal-content">
                                                    <!-- heading modal -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Apakah anda yakin ingin menghapus?</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body" align="right">
                                                        <form action="{{route('hapus_pegawai', $p)}}" method="post">
                                                            @csrf
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <a class="nav-icon fas fa-trash" style="color:dimgray" href="{{route('hapus_pegawai', $p)}}"></a> -->
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
<script>
    $(function() {
        $("#table").DataTable();
    });
</script>
<!-- /.card -->

@endsection('content')