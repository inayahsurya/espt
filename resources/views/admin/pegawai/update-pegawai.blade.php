@extends('partials.master')
@section('title', 'Update-Pegawai')
@section('content')
@section('content-title', 'Update Pegawai')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <a class="fa fa-arrow-left mb-3" href="{{route('pegawai')}}" style="color: dimgray"></a>
                <form class="form-horizontal" action="{{route('update_pegawai', $pegawai)}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="namaPegawai" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('namaPegawai') is-invalid @enderror" name="namaPegawai" placeholder="Nama" value="{{ $pegawai->nama }}">
                            {!! $errors->first('namaPegawai','<span class="invalid-feedback">Nama pegawai tidak boleh kosong.</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nipPegawai" class="col-sm-2 col-form-label">NIP</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control @error('nipPegawai') is-invalid @enderror" name="nipPegawai" placeholder="NIP" value="{{$pegawai->nip}}">
                            {!! $errors->first('nipPegawai','<span class="invalid-feedback">NIP pegawai tidak boleh kosong.</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="statusPegawai" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control select2bs4" name="statusPegawai" style="width: auto;" value="{{old ('statusPegawai')}}">
                                <option value="1" {{$pegawai->status == 1 ? 'selected' : ''}}>PNS</option>
                                <option value="2" {{$pegawai->status == 2 ? 'selected' : ''}}>PTT</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="golonganPegawai" class="col-sm-2 col-form-label">Pangkat/Golongan</label>
                        <div class="col-sm-10">
                            <select class="form-control select2bs4 @error('golonganPegawai') is-invalid @enderror" name="golonganPegawai" style="width: 100%;">
                                @foreach($golonganPangkat as $g)
                                <option value="{{$g->tingkat}}" {{$g->tingkat == $pegawai->id_golongan_pangkat ? 'selected' : ''}}>{{$g->pangkat}} ({{$g->golongan}})</option>
                                @endforeach
                            </select>
                            {!! $errors->first('golonganPegawai','<span class="invalid-feedback">Golongan pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jabatanPegawai" class="col-sm-2 col-form-label">Jabatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('jabatanPegawai') is-invalid @enderror" name="jabatanPegawai" placeholder="Jabatan" value="{{$pegawai->jabatan}}">
                            {!! $errors->first('jabatanPegawai','<span class="invalid-feedback">Jabatan pegawai tidak boleh kosong.</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bidangPegawai" class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                            <!-- <input type="text" class="form-control" id="bidangPegawai" placeholder="Bidang"> -->
                            <select class="form-control select2bs4 @error('bidangPegawai') is-invalid @enderror" name="bidangPegawai" style="width: 100%;">
                                @foreach($bidangs as $b)
                                <option value="{{ $b->id }}" {{$b->id == $pegawai->id_bidang ? 'selected' : ''}}>{{ $b->nama_bidang }}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('bidangPegawai','<span class="invalid-feedback">Bidang pegawai tidak boleh kosong.</span>') !!}
                        </div>
                    </div>
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>
</div>
            <!-- /.card-body -->
            </form>
        </div>
    </div>
    <!-- /.card -->
</div>
@endsection('content')