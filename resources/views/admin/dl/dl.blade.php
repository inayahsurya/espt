@extends('partials.master')
@section('title', 'Laporan Dinas Luar')
@section('content')
@section('content-title', 'Laporan Dinas Luar')

<div class="row">
    <div class="col-12">

        <div class="card">

            <div class="card-body table-responsive p-0">
                <table class="table ">
                    <thead>
                        <tr>
                            <th>No. Surat</th>
                            <th>Tgl Keluar</th>
                            <th>Pegawai</th>
                            <th>Tujuan</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($spt as $s)
                        <tr>
                            <td><?php if ($s->nomor_surat) { ?>
                                    094/{{$s->nomor_surat}}/114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                <?php } else { ?>
                                    094/ &nbsp; &nbsp; &nbsp; /114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                <?php } ?>
                            </td>
                            <td>{{Date::parse($s->laporanDl->tgl_keluar)->format('d F Y')}}</td>
                            <td>
                                @foreach($s->pegawai as $p)
                                {{$p->nama}} <br>
                                @endforeach
                            </td>
                            <td>{{Str::limit($s->tujuan, 80)}}</td>
                            <td>
                                <?php
                                $ket = $s->laporanDl->keterangan;
                                if ($ket == 0) {
                                    $ket = "Belum dibuat";
                                } else if ($ket == 1) {
                                    $ket = "Sudah dibuat";
                                } else if ($ket == 2) {
                                    $ket = "Menunggu konfirmasi";
                                } else {
                                    $ket = "Sudah dikonfirmasi";
                                }
                                ?>
                                {{$ket}}
                            </td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a class="fas fa-eye" style="color:dimgray" href="{{route('detail_dl', $s)}}"></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="nav-icon fas fa-archive" style="color:dimgray" href="{{route('arsip_dl', $s)}}"></a>
                                    </li>
                                </ul>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

</div>

@endsection('content')

<style type="text/css">
    body {
        font-family: Arial, Helvetica, sans-serif;
    }
</style>