@extends('partials.master')
@section('title', 'Tambah-Admin')
@section('content')
@section('content-title', 'Tambah Admin')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <a class="fa fa-arrow-left mb-3" href="{{route('admin')}}" style="color: dimgray"></a>
                <form class="form-horizontal" action="{{route('tambah_admin')}}" method="post">
                    @csrf

                    <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{old ('username')}}" onkeyup="nospaces(this)">
                            {!! $errors->first('username','<span class="invalid-feedback">Username tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="nip" placeholder="Email" value="{{old ('email')}}">
                            @if ($error = $errors->first('email'))
                            <span class="invalid-feedback">
                                {{ $error }}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password">
                            @if ($error = $errors->first('password'))
                            <span class="invalid-feedback">
                                {{ $error }}
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="passwordConfirmation" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation" placeholder="Password Confirmation">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bidangAdmin" class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                            <!-- <input type="text" class="form-control" id="bidangAdmin" placeholder="Bidang"> -->
                            <select class="form-control select2bs4 @error('bidangAdmin') is-invalid @enderror" name="bidangAdmin" style="width: 100%;">
                                <option></option>
                                @foreach($bidang as $b)
                                <option value="{{$b->id}}" {{old('bidangAdmin') == $b->id ? 'selected' : ''}}>{{$b->nama_bidang}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('bidangAdmin','<span class="invalid-feedback">Bidang admin tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>

    <script>
        $(function() {
            $(":input").inputmask();
        });

        function nospaces(t) {
            if (t.value.match(/\s/g)) {
                alert('Tidak boleh menggunakan spasi!');
                t.value = t.value.replace(/\s/g, '');
            }
        }
    </script>

    @endsection('content')