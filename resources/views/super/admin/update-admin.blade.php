@extends('partials.master')
@section('title', 'Update-Admin')
@section('content')
@section('content-title', 'Update Admin')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <a class="fa fa-arrow-left mb-3" href="{{route('admin')}}" style="color: dimgray"></a>
                <form class="form-horizontal" action="{{route('update_admin', $admin)}}" method="post">
                    @csrf

                    <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{$admin->name}}" onkeyup="nospaces(this)">
                            {!! $errors->first('username','<span class="invalid-feedback">Username tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="nip" placeholder="Email" value="{{$admin->email}}">
                            {!! $errors->first('email','<span class="invalid-feedback">Email pegawai tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bidangAdmin" class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                            <select class="form-control select2bs4 @error('bidangAdmin') is-invalid @enderror" name="bidangAdmin" style="width: 100%;" value="{{old ('bidangAdmin')}}">
                                <option></option>
                                @foreach($bidang as $b)
                                <option value="{{$b->id}}" {{$admin->bidang->id == $b->id ? 'selected' : ''}} >{{$b->nama_bidang}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('bidangAdmin','<span class="invalid-feedback">Bidang admin tidak boleh kosong!</span>') !!}
                        </div>
                    </div>
                    <div class="form-group-row">
                        <a href="{{ route('update_password_admin', $admin) }}">Ganti password?</a>
                    </div>
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>

    <script>
        $(function() {
            $(":input").inputmask();
        });

        function nospaces(t) {
            if (t.value.match(/\s/g)) {
                alert('Sorry, you are not allowed to enter any spaces');
                t.value = t.value.replace(/\s/g, '');
            }
        }
    </script>

    @endsection('content')