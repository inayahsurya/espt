@extends('partials.master')
@section('title', 'Update-Admin')
@section('content')
@section('content-title', 'Update Admin')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <div class="card-body">
                <a class="fa fa-arrow-left mb-3" href="{{route('update_admin', $admin)}}" style="color: dimgray"></a>
                <form class="form-horizontal" action="{{route('update_password_admin', $admin)}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password lama</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control @error('passwordLama') is-invalid @enderror" name="passwordLama" placeholder="Password lama">
                            @if ($error = $errors->first('passwordLama'))
                            <span class="invalid-feedback">
                                {{ $error }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password baru</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password baru">
                            @if ($error = $errors->first('password'))
                            <span class="invalid-feedback">
                                {{ $error }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="passwordConfirmation" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation" placeholder="Password Confirmation">
                        </div>
                    </div>
                    <ul class="list-inline float-right">
                        <li class="list-inline-item">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </li>
                    </ul>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
    <script>
        var msg = '{{Session::get('alert')}}';
        var exist = '{{Session::has('alert')}}';
        if (exist) {
            alert(msg);
        }

        $(function() {
            $(":input").inputmask();
        });

        function nospaces(t) {
            if (t.value.match(/\s/g)) {
                alert('Sorry, you are not allowed to enter any spaces');
                t.value = t.value.replace(/\s/g, '');
            }
        }
    </script>

    @endsection('content')