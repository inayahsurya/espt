@extends('partials.master')
@section('title', 'Admin')
@section('content')
@section('content-title', 'Admin')
<div class="row">
    <div class="col-12 pb-3">
        <a class="btn btn-success" href="{{route('tambah_admin')}}">Tambah admin</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body table-responsive p-0">
                <table id="table " class="table">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Bidang</th>
                            <th >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($admin as $a)
                        <tr>
                            <td>{{ $a->name }}</td>
                            <td>{{ $a->email }}</td>
                            <td>{{ $a->bidang->nama_bidang }}</td>
                            <td >
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a class="nav-icon fas fa-edit" style="color:dimgray" href="{{route('update_admin', $a)}}"></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <!-- Tombol untuk menampilkan modal-->
                                        <a class="nav-icon fas fa-trash" data-toggle="modal" data-target="#deleteModal{{ $a->id }}" style="color:dimgray"></a>
                                        <!-- Modal -->
                                        <div id="deleteModal{{ $a->id }}" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- konten modal-->
                                                <div class="modal-content">
                                                    <!-- heading modal -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Apakah anda yakin ingin menghapus?</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body" align="right">
                                                        <form action="{{route('hapus_admin', $a)}}" method="post">
                                                            @csrf
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
<script>
    
</script>
<!-- /.card -->

@endsection('content')