<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    <img src="{{ asset('assets/img/logo2.png')}}" alt="E-SPT Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light"><b>E-SPT</b></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('assets/img/user.png') }}" class="img-circle" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">
          @if(Auth::guard('admin')->user())
          {{Auth::guard('admin')->user()->name}}
          @endif
          @if(Auth::guard('superadmin')->user())
          {{Auth::guard('superadmin')->user()->name}}
          @endif
        </a>
      </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        @if(Auth::guard('admin')->user())
        <li class="nav-item">
          <a href="{{route('spt')}}" class="nav-link">
            <i class="nav-icon fas fa-envelope-open-text"></i>
            <p>
              SPT
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('dl')}}" class="nav-link">
            <i class="nav-icon fas fa-file-invoice"></i>
            <p>
              Laporan Dinas Luar
            </p>
          </a>
        </li>
        @endif
        @if(Auth::guard('superadmin')->user())
        <li class="nav-item">
          <a href="{{route('admin')}}" class="nav-link">
            <i class="nav-icon fas fa-users-cog"></i>
            <p>
              Admin
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('dasar')}}" class="nav-link">
            <i class="nav-icon fas fa-book-open"></i>
            <p>
              Dasar SPT
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('golongan')}}" class="nav-link">
            <i class="nav-icon fas fa-briefcase"></i>
            <p>
              Golongan dan Pangkat
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('bidang')}}" class="nav-link">
            <i class="nav-icon fas fa-sitemap"></i>
            <p>
              Struktur Organisasi
            </p>
          </a>
        </li>
        
        @endif
        <li class="nav-item">
          <a href="{{route('pegawai')}}" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p>
              Pegawai
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>