<footer class="main-footer">
  <strong>Copyright &copy; 2020 <img src="{{ asset('assets/img/kominfo.png')}}" width="15px"> <a href="http://www.kominfo.jatimprov.go.id" target="_blank">Dinas Komunikasi dan Informatika Jawa Timur</a>.</strong> All rights
  reserved.
</footer>