<!-- Navbar -->
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
        <a href="#" class="navbar-brand">
            <img src="{{ asset('assets/img/logo2.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">E-SPT</span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                @if (Auth::guard('user')->user())
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('spt_user')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('profile', Auth::guard('user')->user())}}">Profile</a>
                    </li>
                @endif
            </ul>

            <!-- Right navbar links -->
            <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                <!-- Messages Dropdown Menu -->
                <?php if (Auth::guard('user')->user()) { ?>
                    <li class="nav-item"><span class="nav-link">Selamat bertugas, {{Auth::guard('user')->user()->name}}</span></li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('logout')}}">
                            <i class="fa fa-sign-out-alt"></i>
                        </a>
                    </li>
                <?php } else { ?>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{route('login')}}">
                            Login
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
</nav>
<!-- /.navbar -->