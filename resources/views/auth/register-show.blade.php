@extends('partials.master-user')
@section('title', 'E-SPT')
@section('content')
@section('content-title', ' ')
<div class="container">
    <div class="row">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-3"></div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-6">
            <form class="form-horizontal" action="{{route('register_show')}}" method="get">
                @csrf
                <div class="input-group input-group-lg">
                    <input type="number" name="nipRegister" class="form-control" placeholder="Masukkan NIP anda">
                    <span class="input-group-append">
                        <button type="submit" class="btn btn-info btn-flat">Cari</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('tambah_user', $peg->id)}}" class="form-horizontal" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="namaPegawai" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="{{$peg->nama}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nipPegawai" class="col-sm-2 col-form-label">NIP</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="nip" value="{{$peg->nip}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pangkatPegawai" class="col-sm-2 col-form-label">Pangkat / Gol</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="{{$peg->golongan_pangkat->pangkat}} ({{$peg->golongan_pangkat->golongan}})" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jabatanPegawai" class="col-sm-2 col-form-label">Jabatan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control " value="{{$peg->jabatan}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="col-sm-2 col-form-label">Status</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control " <?php if ($peg->status == 1) { ?> value="PNS" <?php } else { ?> value="PTT" <?php } ?> readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="bidang" class="col-sm-2 col-form-label">Bidang</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control " value="{{$peg->bidang ? $peg->bidang->nama_bidang : ''}}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class="col-sm-2 col-form-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{old('username')}}">
                                {!! $errors->first('username','<span class="invalid-feedback">Username tidak boleh kosong!</span>') !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{old('email')}}">
                                @if ($error = $errors->first('email'))
                                <span class="invalid-feedback">
                                    {{ $error }}
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password">
                                @if ($error = $errors->first('password'))
                                <span class="invalid-feedback">
                                    {{ $error }}
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password_confirmation" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation" placeholder="Konfirmasi Password">
                                {!! $errors->first('password_confirmation','<span class="invalid-feedback">Password salah</span>') !!}
                            </div>
                        </div>
                        <ul class="list-inline float-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')