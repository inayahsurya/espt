@extends('partials.master-user')
@section('title', 'E-SPT')
@section('content')
@section('content-title', ' ')
<div class="container">
    <div class="row">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-3"></div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-6">
            <form class="form-horizontal" action="{{route('register_show')}}" method="get">
                @csrf
                <div class="input-group input-group-lg">
                    <input type="number" name="nipRegister" class="form-control" placeholder="Masukkan NIP anda">
                    <span class="input-group-append">
                        <button type="submit" class="btn btn-info btn-flat">Cari</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <h4 style="color: dimgray;" align="center">{{$msg}}</h4>
        </div>
    </div>
</div>
@endsection('content')