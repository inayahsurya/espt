@extends('partials.master-user')
@section('title', 'E-SPT')
@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            @if(!empty($spt))
            @section('content-title', 'Surat Perintah Tugas')
            @foreach($spt as $s)
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0"><b>Nomor Surat : <?php if ($s->nomor_surat) { ?>
                                    094/{{$s->nomor_surat}}/114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                <?php } else { ?>
                                    094/ &nbsp; &nbsp; &nbsp; /114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                <?php } ?></b></h5>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title"><b>Tujuan SPT</b></h5>

                        <p class="card-text" align="justify">{{Str::limit($s->tujuan, 160)}}</p>
                        <ul class="list-inline">
                            <?php
                            if ($s->laporanDl->keterangan == 0) {
                            ?>
                                <li class="list-inline-item"><a href="{{route('tambah_laporan_dl', $s->id)}}" class="btn btn-primary">Buat Laporan</a></li>
                            <?php
                            } else if ($s->laporanDl->keterangan == 1) {
                            ?>
                                <li class="list-inline-item"><a href="{{route('detail_laporan_dl', $s)}}" class="btn btn-secondary">Lihat Laporan</a></li>
                            <?php
                            }
                            ?>
                            <li class="list-inline-item"><button class="btn btn-secondary" data-toggle="modal" data-target="#sptModal{{ $s->id }}" style="color: white;"> Lihat SPT</button></li>
                        </ul>
                        <!-- Modal -->
                        <div class="modal fade" id="sptModal{{$s->id}}" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Surat Perintah Tugas - <?php if ($s->nomor_surat) { ?>
                                                094/{{$s->nomor_surat}}/114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                            <?php } else { ?>
                                                094/ &nbsp; &nbsp; &nbsp; /114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                            <?php } ?></h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <table width="100%" class="table table-borderless ">
                                            <tr>
                                                <td align="center">
                                                    <b><u>SURAT PERINTAH TUGAS</u></b><br>
                                                    Nomor : <?php if ($s->nomor_surat) { ?>
                                                        094/{{$s->nomor_surat}}/114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                                    <?php } else { ?>
                                                        094/ &nbsp; &nbsp; &nbsp; /114.{{$s->id_bidang}}/{{ Date::parse($s->tgl_keluar)->format('Y')}}
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="table table-borderless ">
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <b>Dasar</b>
                                                </td>
                                                <td style="vertical-align: top;"><b>:</b></td>
                                                <td>
                                                    <table class="table table-borderless ">
                                                        @foreach($dasar as $d)
                                                        <tr>
                                                            <td style="vertical-align: top;">
                                                                {{$d->nomor}}.
                                                            </td>
                                                            <td align="justify">
                                                                {{$d->deskripsi}}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="table table-borderless ">
                                            <tr>
                                                <td align="center">
                                                    <b><u>M E M E R I N T A H K A N</u></b>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table table-borderless ">
                                            <tr>
                                                <td class="vertical-align: top;">
                                                    <b>Kepada</b>
                                                </td>
                                                <td style="vertical-align: top;"><b>:</b></td>
                                                <td style="width: 90%">
                                                    <table class="table table-borderless">
                                                        <?php
                                                        $i = 1 ?>
                                                        @foreach($s->pegawai as $p)
                                                        <tr>
                                                            <td style="width: 4%">{{$i++}}. </td>
                                                            <td style="width: 21%">Nama</td>
                                                            <td style="width: 5%"> : </td>
                                                            <td style="width: 70%">{{$p->nama}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>NIP</td>
                                                            <td> : </td>
                                                            <td>{{$p->nip}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>Pangkat/Gol</td>
                                                            <td> : </td>
                                                            <td>
                                                                <?php
                                                                if ($p->golongan_pangkat->tingkat == 0) {
                                                                    echo "-";
                                                                } else { ?>
                                                                    {{$p->golongan_pangkat->pangkat}} ({{$p->golongan_pangkat->golongan}})
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>Jabatan</td>
                                                            <td> : </td>
                                                            <td>{{$p->jabatan}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table table-borderless" style="text-align: justify">
                                            <tr>
                                                <td width="10%" style="vertical-align: top;">
                                                    <b>Untuk </b>
                                                </td>
                                                <td style="vertical-align: top;"><b>:</b></td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td style="text-align: justify">
                                                                {{$s->tujuan}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-lg-12 col-md-12" style="text-align: center">
            <h5>Tidak ada perintah tugas</h5>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection('content')