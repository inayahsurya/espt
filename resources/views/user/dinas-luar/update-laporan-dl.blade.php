@extends('partials.master-user')
@section('title', 'E-SPT')
@section('content')
@section('content-title', '')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
                <h3 class="pb-3">Update Laporan Dinas Luar</h3>
                <div class="card">
                    <div class="card-body">

                        <a class="fa fa-arrow-left mb-3" href="{{route('spt_user')}}" style="color: dimgray"></a>
                        <form class="form-horizontal" action="{{route('update_laporan_dl', $spt->id)}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="daerahTujuan" class="col-sm-2 col-form-label">Daerah Tujuan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('daerahTujuan') is-invalid @enderror" name="daerahTujuan" placeholder="Daerah Tujuan" value="{{$spt->laporanDl->daerah_tujuan}}">
                                    {!! $errors->first('daerahTujuan','<span class="invalid-feedback">Daerah tujuan tidak boleh kosong!</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktuPelaksanaan" class="col-sm-2 col-form-label">Waktu Pelaksanaan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('waktuPelaksanaan') is-invalid @enderror" name="waktuPelaksanaan" placeholder="Waktu Pelaksanaan" value="{{$spt->laporanDl->waktu_pelaksanaan}}">
                                    {!! $errors->first('waktuPelaksanaan','<span class="invalid-feedback">Waktu pelaksanaan tidak boleh kosong!</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hadirDalamPertemuan" class="col-sm-2 col-form-label">Hadir Dalam Pertemuan</label>
                                <div class="col-sm-10">
                                    <textarea class="textarea @error('hadirDalamPertemuan') is-invalid @enderror" id="hadirDalamPertemuan" name="hadirDalamPertemuan" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    {!! $errors->first('hadirDalamPertemuan','<span class="invalid-feedback">Field hadir dalam pertemuan tidak boleh kosong!</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hasilDalamPertemuan" class="col-sm-2 col-form-label">Hasil Dalam Pertemuan</label>
                                <div class="col-sm-10">
                                    <textarea class="textarea @error('hasilDalamPertemuan') is-invalid @enderror" id="hasilDalamPertemuan" name="hasilDalamPertemuan" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    {!! $errors->first('hasilDalamPertemuan','<span class="invalid-feedback">Field hasil dalam pertemuan tidak boleh kosong!</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="temuanMasalah" class="col-sm-2 col-form-label">Temuan Masalah</label>
                                <div class="col-sm-10">
                                    <textarea class="textarea @error('temuanMasalah') is-invalid @enderror" id="temuanMasalah" name="temuanMasalah" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    {!! $errors->first('temuanMasalah','<span class="invalid-feedback">Field temuan masalah tidak boleh kosong!</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="saranTindak" class="col-sm-2 col-form-label">Saran Tindak</label>
                                <div class="col-sm-10">
                                    <textarea class="textarea @error('saranTindak') is-invalid @enderror" id="saranTindak" name="saranTindak" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    {!! $errors->first('saranTindak','<span class="invalid-feedback">Field saran tindak tidak boleh kosong!</span>') !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <ul class="list-inline float-right">
                                    <li class="list-inline-item">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // Summernote
        $('.textarea').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['view', ['fullscreen', 'codeview', 'help']],
            ],
        });

        var contentHadir = {!! json_encode($spt->laporanDl->kehadiran) !!};
        $('#hadirDalamPertemuan').summernote('code', contentHadir);

        var contentHasil = {!! json_encode($spt->laporanDl->hasil) !!};
        $('#hasilDalamPertemuan').summernote('code', contentHasil);

        var contentMasalah = {!! json_encode($spt->laporanDl->masalah) !!};
        $('#temuanMasalah').summernote('code', contentMasalah);

        var contentSaran = {!! json_encode($spt->laporanDl->saran) !!};
        $('#saranTindak').summernote('code', contentSaran);
    })
</script>
@endsection('content')