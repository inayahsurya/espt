<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=V, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>

<body>
    <table>
        <tr>
            <td align="center">
                <b style="font-size: 14pt"><u style="font-size: 14pt">LAPORAN DINAS LUAR</u></b> <br>
                Nomor : <?php if ($spt->nomor_surat) { ?>
                    094/{{$spt->nomor_surat}}/114.{{$spt->id_bidang}}/{{ Date::parse($spt->tgl_keluar)->format('Y')}}
                <?php } else { ?>
                    094/ &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; /114.{{$spt->id_bidang}}/{{ Date::parse($spt->tgl_keluar)->format('Y')}}
                <?php } ?>
            </td>
        </tr>
    </table>
    <br><br><br>
    <table>
        <tr>
            <td style="width:6%"><b>I.</b></td>
            <td style="width:22%"><b>DASAR</b></td>
            <td style="width:2%">:</td>
            <td style="width:70%">
                <table>
                    @foreach($dasar as $d)
                    <tr>
                        <td style="width: 5%">{{$d->nomor}}.</td>
                        <td align="justify" style="width: 95%">{{$d->deskripsi}}</td>
                    </tr>
                    @endforeach
                </table>
            </td>
        </tr>
        <tr><td></td><td></td><td></td><td></td></tr>
        <tr>
            <td><b>II.</b></td>
            <td><b>MAKSUD TUJUAN</b></td>
            <td>:</td>
            <td align="justify">{{$spt->tujuan}}</td>
        </tr>
        <tr><td></td><td></td><td></td><td></td></tr>
        <tr>
            <td><b>III.</b></td>
            <td><b>NAMA PETUGAS</b></td>
            <td>:</td>
            <td><?php $i = 1 ?>
                <table>
                    @foreach($peg as $p)
                    <tr>
                        <td style="width: 5%">{{$i++}}.</td>
                        <td style="width: 95%">{{$p->nama}}</td>
                    </tr>
                    @endforeach
                </table>
            </td>
        </tr>
        <tr><td></td><td></td><td></td><td></td></tr>
        <tr>
            <td><b>IV.</b></td>
            <td><b>DAERAH TUJUAN</b></td>
            <td>:</td>
            <td>{{$spt->laporanDl->daerah_tujuan}}</td>
        </tr>
        <tr><td></td><td></td><td></td><td></td></tr>
        <tr>
            <td><b>V.</b></td>
            <td><b>WAKTU PELAKSANAAN</b></td>
            <td>:</td>
            <td>{{$spt->laporanDl->waktu_pelaksanaan}}</td>
        </tr>
        <tr><td></td><td></td><td></td><td></td></tr>
        <tr>
            <td><b>VII.</b></td>
            <td style="text-align: left"><b>HADIR DALAM PERTEMUAN</b></td>
            <td>:</td>
            <td style="padding-top: -14px">{!!$spt->laporanDl->kehadiran!!}</td>
        </tr>
        <tr>
            <td><b>VII.</b></td>
            <td style="text-align: left"><b>HASIL DALAM PERTEMUAN</b></td>
            <td>:</td>
            <td style="padding-top: -14px; text-align: justify">{!!$spt->laporanDl->hasil!!}</td>
        </tr>
        <tr>
            <td><b>VIII.</b></td>
            <td><b>TEMUAN MASALAH</b></td>
            <td>:</td>
            <td style="padding-top: -14px; text-align: justify">{!!$spt->laporanDl->masalah!!}</td>
        </tr>
        <tr>
            <td><b>XI.</b></td>
            <td><b>SARAN TINDAK</b></td>
            <td>:</td>
            <td style="padding-top: -14px; text-align: justify">{!!$spt->laporanDl->saran!!}</td>
        </tr>
    </table>
    <br><br><br><br>
    <table style="width: 100%; page-break-inside: avoid">
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td style="text-align: right">Surabaya, {{Date::parse($spt->laporanDl->created_at)->format('d F Y')}}</td>
                    </tr>
                </table>
                <table style="margin-left: 26px; width: 100%; text-align: justify">
                    <tr>
                        <td>Penanggung Jawab </td>
                        <td><br><br><br></td>
                    </tr>
                    <tr>
                        <td style="padding-top: 30"><u>{{$peg[0]->nama}}</u><br>NIP. {{$peg[0]->nip}}</td>
                        <td style="padding-top: 30; text-align: right">Tanda Tangan : ............................................</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

<style type="text/css">
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        margin-bottom: 10px;
    }

    td {
        vertical-align: top;
    }
</style>

</html>