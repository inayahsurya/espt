@extends('partials.master-user')
@section('title', 'E-SPT')
@section('content')
@section('content-title', '')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <h3 class="pb-3">Detail Laporan Dinas Luar</h3>
        </div>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="card col-md-10">
                <div class="card-body">
                    <a class="fa fa-arrow-left mb-3" href="{{route('spt_user')}}" style="color: dimgray"></a>
                    <table style="width: 100%">
                        <tr>
                            <td style="text-align: center">
                                <b><u>LAPORAN DINAS LUAR</u></b> <br>
                                Nomor : <?php if ($spt->nomor_surat) { ?>
                                    094/{{$spt->nomor_surat}}/114.{{$spt->id_bidang}}/{{ Date::parse($spt->tgl_keluar)->format('Y')}}
                                <?php } else { ?>
                                    094/<h style="color: white;">0000000</h>/114.{{$spt->id_bidang}}/{{ Date::parse($spt->tgl_keluar)->format('Y')}}
                                <?php } ?>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table style="text-align: justify">
                        <tr>
                            <td style="vertical-align: top; width: 20%">
                                <b>Dasar</b>
                            </td>
                            <td style="vertical-align: top; width: 2%">
                                :
                            </td>
                            <td>
                                @foreach($dasar as $d)
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">
                                            <p>{{$d->nomor}}. </p>
                                        </td>
                                        <td align="justify">
                                            {{$d->deskripsi}}
                                        </td>
                                    </tr>
                                </table>
                                @endforeach
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: top;">
                                <b>Maksud Tujuan</b>
                            </td>
                            <td style="vertical-align: top;">
                                :
                            </td>
                            <td>
                                {{$spt->tujuan}}
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <b>Nama Petugas</b>
                            </td>
                            <td style="vertical-align: top">
                                :
                            </td>
                            <td>
                                <?php $i = 1 ?>
                                @foreach($peg as $p)
                                <table>
                                    <tr>
                                        <td>{{$i++}}. </td>
                                        <td>{{$p->nama}}</td>
                                    </tr>
                                </table>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Daerah Tujuan</b>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{$spt->laporanDl->daerah_tujuan}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Waktu Pelaksanaan</b>
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                {{$spt->laporanDl->waktu_pelaksanaan}}
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <b>Hadir Dalam Pertemuan</b>
                            </td>
                            <td style="vertical-align: top;">
                                :
                            </td>
                            <td>
                                {!!$spt->laporanDl->kehadiran!!}
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <b>Hasil Dalam Pertemuan</b>
                            </td>
                            <td style="vertical-align: top;">
                                :
                            </td>
                            <td>
                                {!!$spt->laporanDl->hasil!!}
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <b>Temuan Masalah</b>
                            </td>
                            <td style="vertical-align: top;">
                                :
                            </td>
                            <td>
                                {!!$spt->laporanDl->masalah!!}
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <b>Saran Tindak</b>
                            </td>
                            <td style="vertical-align: top;">
                                :
                            </td>
                            <td>
                                {!!$spt->laporanDl->saran!!}
                            </td>
                        </tr>
                    </table>
                    <div class="modal-footer">
                        <form action="{{ route('update_laporan_dl', $spt)}}" method="get">
                            <button type="submit" class="btn btn-secondary"> Edit </button>
                        </form>
                        <form action="{{ route('cetak_laporan_dl', $spt)}}" target="_blank" method="post">
                            @csrf
                            <button type="submit" class="btn btn-primary">Cetak</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')