@extends('partials.master-user')
@section('title', 'Ubah Password')
@section('content')
@section('content-title', ' ')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
                <h3 class="pb-3">Profile</h3>
                <div class="card">
                    <div class="card-body">
                        <a class="fa fa-arrow-left mb-3" href="{{route('update_profile', $user)}}" style="color: dimgray"></a>
                        <form action="{{route('update_password_user', $user)}}" class="form-horizontal" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="password" class="col-sm-2 col-form-label">Password lama</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control @error('passwordLama') is-invalid @enderror" name="passwordLama" placeholder="Password lama">
                                    @if ($error = $errors->first('passwordLama'))
                                    <span class="invalid-feedback">
                                        {{ $error }}
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-sm-2 col-form-label">Password baru</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password baru">
                                    @if ($error = $errors->first('password'))
                                    <span class="invalid-feedback">
                                        {{ $error }}
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="passwordConfirmation" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="password_confirmation" placeholder="Password Confirmation">
                                </div>
                            </div>
                            <div>
                                <ul class="list-inline float-right">
                                    <li class="list-inline-item">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if (exist) {
        alert(msg);
    }
</script>
@endsection('content')