@extends('partials.master-user')
@section('title', 'Edit Profile')
@section('content')
@section('content-title', ' ')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
                <h3 class="pb-3">Profile</h3>
                <div class="card">
                    <div class="card-body">
                        <a class="fa fa-arrow-left mb-3" href="{{route('profile', $user)}}" style="color: dimgray"></a>
                        <form action="{{route('update_profile', $user)}}" class="form-horizontal" method="post">
                            @csrf
                            <div class="form-group row">
                                <label for="namaPegawai" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="{{$user->pegawai->nama}}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nipPegawai" class="col-sm-2 col-form-label">NIP</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="nip" value="{{$user->pegawai->nip}}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pangkatPegawai" class="col-sm-2 col-form-label">Pangkat / Gol</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="{{ $user->pegawai->golongan_pangkat ? $user->pegawai->golongan_pangkat->pangkat .' ('. $user->pegawai->golongan_pangkat->golongan.')' : ''}}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jabatanPegawai" class="col-sm-2 col-form-label">Jabatan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control " value="{{$user->pegawai->jabatan}}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-sm-2 col-form-label">Status</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control " <?php if ($user->pegawai->status == 1) { ?> value="PNS" <?php } else { ?> value="PTT" <?php } ?> readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bidang" class="col-sm-2 col-form-label">Bidang</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control " value="{{$user->pegawai->bidang ? $user->pegawai->bidang->nama_bidang : ''}}" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-sm-2 col-form-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{$user->name}}">
                                    {!! $errors->first('username','<span class="invalid-feedback">Username tidak boleh kosong!</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email"  value="{{$user->email}}">
                                    @if ($error = $errors->first('email'))
                                    <span class="invalid-feedback">
                                        {{ $error }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div>
                                <a href="{{route('update_password_user', $user)}}">ganti password ?</a>
                            </div>
                            <div>
                                <ul class="list-inline float-right">
                                    <li class="list-inline-item">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>
@endsection('content')