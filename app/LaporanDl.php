<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanDl extends Model
{
    public function spt()
    {
        return $this->belongsTo('App\Spt', 'spt_id');
    }

    public function arsipDl()
    {
        return $this->hasMany('App\ArsipDl', 'id_dl');
    }
}
