<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArsipSpt extends Model
{
    public function spt(){
        return $this->belongsTo('App\Spt', 'id_spt');
    }
}
