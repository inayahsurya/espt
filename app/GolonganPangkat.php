<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GolonganPangkat extends Model
{
    protected $primaryKey = 'tingkat';

    public function pegawai(){
        return $this->hasMany('App\Pegawai', 'tingkat', 'id_golongan_pangkat');
    }
}
