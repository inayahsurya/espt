<?php

if (!function_exists('tglMulai')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function tglMulai($date)
    {
        $mm = substr($date, 0, 2);
        $dd = substr($date, 3, 2);
        $yyyy = substr($date, 6, 4);

        return "$yyyy-$mm-$dd";
    }

    
}

if (!function_exists('tglSelesai')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function tglSelesai($date)
    {
        $mm = substr($date, 13, 2);
        $dd = substr($date, 16, 2);
        $yyyy = substr($date, 19, 4);

        return "$yyyy-$mm-$dd";
    } 
}

if (!function_exists('mmddyyyyToYmd')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function mmddyyyyToYmd($date)
    {
        $mm = substr($date, 0, 2);
        $dd = substr($date, 3, 2);
        $yyyy = substr($date, 6, 4);

        return "$yyyy-$mm-$dd";
    } 
}

if (!function_exists('tglKeluar')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function tglKeluar($date)
    {
        $dd = '01';
        $mm = substr($date, 0, 2);
        $yyyy = substr($date, 3, 4);

        return "$yyyy-$mm-$dd";
    } 
}

if (!function_exists('tommyyyy')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function tommyyyy($date)
    {
        $mm = substr($date, 5, 2);
        $yyyy = substr($date, 0, 4);

        return "$mm-$yyyy";
    } 
}