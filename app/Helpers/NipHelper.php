<?php

if (!function_exists('printNip')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function printNip($nip, $status)
    {
        if ($status == 1) {
            $tgllahir = substr($nip, 0, 8);
            $tglpns = substr($nip, 8, 6);
            $kelamin = substr($nip, 14, 1);
            $index = substr($nip, 15, 3);

            $nip = "$tgllahir $tglpns $kelamin $index";
        }
        else if ($status == 2) {
            $instansi = substr($nip, 0, 3);
            $tgllhr = substr($nip, 3, 8);
            $tglmsk = substr($nip, 11, 6);
            $index = substr($nip, 17, 4);

            $nip = "$instansi $tgllhr $tglmsk $index";
        }

        return $nip;
    }
}
