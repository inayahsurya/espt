<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KepalaBidang extends Model
{

    public function bidang(){
        return $this->belongsTo('App\Bidang', 'id_bidang', 'id');
    }

    public function pegawai(){
        return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
    }
    
}
