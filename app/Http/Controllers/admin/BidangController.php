<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Bidang;
use App\KepalaBidang;
use App\Pegawai;
use Illuminate\Http\Request;

class BidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bidang = Bidang::orderBy('nomor', 'ASC')->get();

        return view('admin.bidang.bidang', compact('bidang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bidang.add-bidang');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nomorBidang' => 'required',
            'namaBidang' => 'required',
        ]);

        $bid = new Bidang();
        $bid->nomor = $request->nomorBidang;
        $bid->nama_bidang =  $request->namaBidang;
        $bid->save();

        return redirect()->route('bidang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Bidang $bidang)
    {
        $peg = Pegawai::orderBy('id_golongan_pangkat', 'DESC')->get();
        return view('admin.bidang.update-bidang', compact('bidang', 'peg'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bidang $bidang)
    {
        $request->validate([
            'nomorBidang' => 'required',
            'namaBidang' => 'required',
        ]);

        $bidang->nomor = $request->nomorBidang;
        $bidang->nama_bidang =  $request->namaBidang;
        $bidang->save();

        if ($bidang->kepala_bidang) {
            $kabid = KepalaBidang::where('id_bidang', $bidang->id)->first();
            $kabid->id_pegawai = $request->pegawai;
            $kabid->save();
        } else {
            $kabid = new KepalaBidang();
            $kabid->id_bidang = $bidang->id;
            $kabid->id_pegawai = $request->pegawai;
            $kabid->save();
        }

        return redirect()->route('bidang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bidang $bidang)
    {
        $bidang->delete();

        return redirect()->route('bidang');
    }
}
