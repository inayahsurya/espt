<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ArsipSpt;
use App\Spt;
use File;
use Illuminate\Http\Request;

class ArsipSptController extends Controller
{
    public function index(Spt $spt){
        // $imgSpt = $spt->arsipSpt;
        $imgSpt = ArsipSpt::where('id_spt', $spt->id)->get();
        // dd($imgSpt);
        return view('admin.spt.arsip-spt', compact('imgSpt', 'spt'));
    }

    public function upload(Request $request, Spt $spt){
        $this->validate($request, [
			'arsipSpt' => 'required|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|max:2048'
        ]);
        
        $file = $request->file('arsipSpt');
        $nama_file = time()."_".$file->getClientOriginalName();

        $tujuan_upload = 'arsip_spt/'.$spt->id;
        $file->move($tujuan_upload,$nama_file);
        
        $arsip = new ArsipSpt();
        $arsip->file = $nama_file;
        $arsip->id_spt = $spt->id;
        $arsip->save();

        return redirect()->back();
    }

    public function destroy(ArsipSpt $arsip){
        if($arsip->count() == 1){
            File::deleteDirectory('arsip_spt/'.$arsip->id_spt);
        } else {
            File::delete('arsip_spt/'.$arsip->id_spt.'/'.$arsip->file);
        }
        
        $arsip->delete();
        return redirect()->back();
    }
}
