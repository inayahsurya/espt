<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Dasar;
use Illuminate\Http\Request;
use DB;

class DasarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $dasars = DB::table('dasars')->get();
        $dasars = Dasar::orderBy('nomor','ASC')->get();
        return view('admin.dasar.dasar', compact('dasars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dasar.add-dasar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nomorDasar' => ['required'],
            'deskripsiDasar' => ['required']
        ]);

        $dasars = new Dasar();
        $dasars->nomor = $request->nomorDasar;
        $dasars->deskripsi = $request->deskripsiDasar;
        $dasars->save();

        return redirect()->route('dasar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dasar $dasar)
    {
        // $dasars = Dasar::find($id);
        return view('admin.dasar.update-dasar', compact('dasar'));
        // dd($dasar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
public function update(Request $request, Dasar $dasar)
    {

        request()->validate([
            'nomorDasar' => ['required'],
            'deskripsiDasar' => ['required']
        ]);

        $dasar->nomor = $request->nomorDasar;
        $dasar->deskripsi = $request->deskripsiDasar;
        $dasar->save();

        return redirect()->route('dasar');
        // dd($dasar);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dasar $dasar)
    {
        // DB::table('dasars')->delete($id);
        $dasar->delete();
        return redirect()->route('dasar');
    }
}
