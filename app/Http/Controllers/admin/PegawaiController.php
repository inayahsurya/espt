<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Pegawai;
use App\Bidang;
use App\GolonganPangkat;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pegawais = Pegawai::all();
        return view('admin.pegawai.pegawai', compact('pegawais'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $golonganPangkat = GolonganPangkat::all();
        $bidangs = Bidang::all();
        return view('admin.pegawai.add-pegawai', compact('bidangs', 'golonganPangkat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'namaPegawai' => ['required'],
            'nipPegawai' => ['required'],
            'golonganPegawai' => 'required',
            'statusPegawai' => ['required']
        ]);

        $pegawai = new Pegawai();
        $pegawai->nama = $request->namaPegawai;
        $pegawai->nip = $request->nipPegawai;
        $pegawai->id_golongan_pangkat = $request->golonganPegawai;
        $pegawai->jabatan = $request->jabatanPegawai;
        $pegawai->id_bidang = $request->bidangPegawai;
        $pegawai->status = $request->statusPegawai;
        $pegawai->save();

        return redirect()->route('pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pegawai $pegawai)
    {
        $golonganPangkat = GolonganPangkat::all();
        $bidangs = Bidang::all();

        return view('admin.pegawai.update-pegawai', compact('pegawai', 'bidangs','golonganPangkat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pegawai $pegawai)
    {
        request()->validate([
            'namaPegawai' => ['required'],
            'nipPegawai' => ['required'],
            'statusPegawai' => ['required']
        ]);

        $pegawai->nama = $request->namaPegawai;
        $pegawai->nip = $request->nipPegawai;
        $pegawai->id_golongan_pangkat = $request->golonganPegawai;
        $pegawai->jabatan = $request->jabatanPegawai;
        $pegawai->id_bidang = $request->bidangPegawai;
        $pegawai->status = $request->statusPegawai;
        $pegawai->save();

        return redirect()->route('pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy(Pegawai $pegawai)
    {
        // DB::table('pegawais')->delete($id);
        $pegawai->delete();
        return redirect()->route('pegawai');
    }
}
