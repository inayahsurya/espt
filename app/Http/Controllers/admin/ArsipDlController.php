<?php

namespace App\Http\Controllers\admin;

use App\ArsipDl;
use App\Http\Controllers\Controller;
use App\Spt;
use Illuminate\Http\Request;
use File;

class ArsipDlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Spt $spt)
    {
        $imgDl = ArsipDl::where('id_dl', $spt->laporanDl->id)->get();
        return view('admin.dl.arsip-dl', compact('imgDl', 'spt'));
    }

    public function upload(Request $request, Spt $spt){
        $this->validate($request, [
			'arsipDl' => 'required|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|max:2048'
        ]);
        
        $file = $request->file('arsipDl');
        $nama_file = time()."_".$file->getClientOriginalName();

        $tujuan_upload = 'arsip_dl/'.$spt->laporanDl->id;
        $file->move($tujuan_upload,$nama_file);
        
        $arsip = new ArsipDl();
        $arsip->file = $nama_file;
        $arsip->id_dl = $spt->laporanDl->id;
        $arsip->save();

        return redirect()->back();
    }

    public function destroy(ArsipDl $arsip){
        if($arsip->count() == 1){
            File::deleteDirectory('arsip_dl/'.$arsip->id_dl);
        } else {
            File::delete('arsip_dl/'.$arsip->id_dl.'/'.$arsip->file);
        }
        $arsip->delete();
        return redirect()->back();
    }
}
