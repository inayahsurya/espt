<?php

namespace App\Http\Controllers\admin;

use App\Bidang;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pegawai;
use App\Dasar;
use App\KepalaBidang;
use App\Spt;
use Auth;
use App\Helper\DateHelper;
use App\LaporanDl;
use TCPDF;
use Jenssegers\Date\Date;

class SptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_bidang = Auth::guard('admin')->user()->id_bidang;
        $dasar = Dasar::all();
        $spt = Spt::where('id_bidang', $id_bidang)
            ->orderBy('updated_at', 'DESC')->get();
        return view('admin.spt.spt', compact('spt', 'dasar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dasar = Dasar::orderBy('nomor', 'ASC')->get();
        $pegawai = Pegawai::orderBy('id_golongan_pangkat', 'DESC')->get();
        return view('admin.spt.add-spt', compact('pegawai', 'dasar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nomorSPT' => 'unique:spts,nomor_surat',
            'pegawaiSPT' => 'required',
            'tujuanSPT' => 'required',
        ]);

        $spt = new Spt();
        $spt->id_bidang = Auth::guard('admin')->user()->id_bidang;
        $spt->nomor_surat = $request->nomorSPT;
        $spt->tgl_keluar = tglKeluar($request->tglKeluar);
        // $spt->tgl_keluar =  Date::parse($spt->tgl_keluar)->format('Y-m-d');

        $spt->tujuan = $request->tujuanSPT;
        $spt->save();

        $pegawai = Pegawai::find($request->pegawaiSPT);
        $spt->pegawai()->attach($pegawai);

        $dl = new LaporanDl();
        $dl->spt_id = $spt->id;
        $dl->save();

        return redirect()->route('spt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Spt $spt)
    {
        $dasar = Dasar::orderBy('nomor', 'ASC')->get();
        $pegawai = Pegawai::orderBy('id_golongan_pangkat', 'DESC')->get();
        return view('admin.spt.update-spt', compact('pegawai', 'dasar', 'spt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Spt $spt)
    {
        $request->validate([
            'nomorSPT' => "unique:spts,nomor_surat,$spt->id",
            'pegawaiSPT' => 'required',
            'tujuanSPT' => 'required',
        ]);


        $spt->nomor_surat = $request->nomorSPT;
        $spt->tgl_keluar = tglKeluar($request->tglKeluar);
        $spt->tujuan = $request->tujuanSPT;
        $spt->pegawai()->sync($request->pegawaiSPT);
        $spt->save();

        return redirect()->route('spt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Spt $spt)
    {
        $spt->delete();
        return redirect()->route('spt');
    }

    public function print(Spt $spt)
    {
        $dasar = Dasar::all();
        $kabid = KepalaBidang::all();
        $kabidTtd = KepalaBidang::where('id_bidang', $spt->id_bidang)->first();
        $kadin = Bidang::where('nama_bidang', 'like', '%' . 'Kepala Dinas' . '%')
            ->orWhere('nama_bidang', 'like', '%' . 'kepala dinas' . '%')->first();
        
        $count = $spt->pegawai->count();
        $peg = $spt->pegawai()->orderBy('id_golongan_pangkat', 'DESC')->get();
        $pId = $peg[0]->id;


        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetTitle('spt-' . $spt->nomor_surat);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();

        if ($kabidTtd && $kadin) {
            foreach ($kabid as $k) {
                $kId = $k->id_pegawai;
                if ($pId == $kId) {
                    $ttd = Pegawai::where('id_bidang', $kadin->id)->first();

                    if ($count < 4) {
                        $view = \View::make('admin.spt.detail-spt-kabid-pdf', compact('spt', 'dasar', 'ttd', 'peg'));
                    } else {
                        $view = \View::make('admin.spt.detail-spt-kabid-pdf2', compact('spt', 'dasar', 'ttd', 'peg'));
                    }
                    break;
                } else {
                    $ttd = KepalaBidang::where('id_bidang', $spt->id_bidang)->first();

                    if ($count < 4) {
                        $view = \View::make('admin.spt.detail-spt-pdf', compact('spt', 'dasar', 'ttd', 'peg'));
                    } else {
                        $view = \View::make('admin.spt.detail-spt-pdf2', compact('spt', 'dasar', 'ttd', 'peg'));
                    }
                }
            }
            $html = $view->render();
            $pdf->writeHTML($html, true, false, true, false, '');
            $pdf->Output('spt-' . $spt->nomor_surat . '.pdf');
        } else {
            return redirect()->back()->with('alert', 'Kepala Bidang / Kepala Dinas tidak ada');
        }
    }
}
