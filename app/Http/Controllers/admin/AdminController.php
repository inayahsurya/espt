<?php

namespace App\Http\Controllers\admin;

use App\Admin;
use App\Bidang;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;

class AdminController extends Controller
{
    public function index()
    {
        $admin = Admin::orderBy('id_bidang', 'ASC')->get();
        return view('super.admin.admin', compact('admin'));
    }

    public function create()
    {
        $bidang = Bidang::orderBy('nomor', 'ASC')->get();
        return view('super.admin.add-admin', compact('bidang'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'email' => 'required|unique:admins,email',
            'password' => 'required|confirmed|min:6',
            'bidangAdmin' => 'required'
        ]);

        $admin = new Admin();
        $admin->name = $request->username;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $admin->id_bidang = $request->bidangAdmin;
        $admin->save();

        return redirect()->route('admin');
    }

    public function edit(Admin $admin)
    {
        $bidang = Bidang::orderBy('nomor', 'ASC')->get();
        return view('super.admin.update-admin', compact('admin', 'bidang'));
    }

    public function update(Request $request, Admin $admin)
    {
        $request->validate([
            'username' => 'required',
            'email' => "required|unique:admins,email,$admin->id",
        ]);

        $admin->name = $request->username;
        $admin->email = $request->email;
        $admin->id_bidang = $request->bidangAdmin;
        $admin->save();

        return redirect()->route('admin');
    }

    public function editPassword(Admin $admin)
    {
        return view('super.admin.update-password', compact('admin'));
    }

    public function changePassword(Request $request, Admin $admin)
    {
        $request->validate([
            'passwordLama' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);

        if (Hash::check($request->passwordLama, $admin->password)) {
            $admin->password = Hash::make($request->password);
            $admin->save();

            return redirect()->back()->with('alert', 'Password berhasil diubah');
        } else {  
            $errors = new MessageBag(['passwordLama' => ['Password yang anda masukkan salah']]);
            return Redirect::back()->withErrors($errors);
        }
    }

    public function destroy(Admin $admin)
    {
        $admin->delete();
        return redirect()->route('admin');
    }

    public function tambah()
    {
        return view('tambah-admin');
    }
}
