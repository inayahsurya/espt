<?php

namespace App\Http\Controllers\admin;

use App\Dasar;
use App\Http\Controllers\Controller;
use App\Spt;
use Illuminate\Http\Request;
use Auth; 

class DinasLuarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_bidang = Auth::guard('admin')->user()->id_bidang;
        $spt = Spt::where('id_bidang', $id_bidang)
            ->orderBy('updated_at', 'DESC')->get();

        return view('admin.dl.dl', compact('spt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Spt $spt)
    {
        $dasar = Dasar::all();
        $peg = $spt->pegawai()->orderBy('id_golongan_pangkat', 'DESC')->get();
        return view('admin.dl.detail-dl', compact('spt', 'dasar', 'peg'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
