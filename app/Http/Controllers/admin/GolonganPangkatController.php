<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\GolonganPangkat;
use Illuminate\Http\Request;

class GolonganPangkatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gol = GolonganPangkat::orderBy('tingkat', 'ASC')->get();

        return view('admin.golongan.golongan', compact('gol'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.golongan.add-golongan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tingkat' => 'required',
            'golongan' => 'required',
            'pangkat' => 'required',
        ]);

        $gol = new GolonganPangkat();
        $gol->tingkat = $request->tingkat;
        $gol->golongan = $request->golongan;
        $gol->pangkat = $request->pangkat;
        $gol->save();

        return redirect()->route('golongan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(GolonganPangkat $gol)
    {
        return view('admin.golongan.update-golongan', compact('gol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GolonganPangkat $gol)
    {
        $request->validate([
            'tingkat' => 'required',
            'golongan' => 'required',
            'pangkat' => 'required',
        ]);

        $gol->tingkat = $request->tingkat;
        $gol->golongan = $request->golongan;
        $gol->pangkat = $request->pangkat;
        $gol->save();

        return redirect()->route('golongan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GolonganPangkat $gol)
    {
        $gol->delete();
        return redirect()->route('golongan');
    }
}
