<?php

namespace App\Http\Controllers;

use App\Pegawai;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|unique:users,email',
            'username' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        $user = new User();
        $user->name = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->id_pegawai = $id;
        $user->save();

        // dd($user);
        $msg = 'Selamat akun anda berhasil terdaftar';

        return view('auth.registered', compact('msg'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $peg = Pegawai::where('nip', $request->nipRegister)->first();
        if ($peg) {
            $user = User::all();

            foreach ($user as $p) {
                if ($peg->id == $p->id_pegawai) {
                    $msg = "Akun anda sudah terdaftar.";
                    return view('auth.registered', compact('msg'));
                }
            }
            return view('auth.register-show', compact('peg'));
        } else {
            $msg = "Pegawai tidak ada";
            return view('auth.registered', compact('msg'));
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
