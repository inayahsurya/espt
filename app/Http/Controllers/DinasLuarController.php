<?php

namespace App\Http\Controllers;

use App\Dasar;
use App\LaporanDl;
use App\Spt;
use Barryvdh\DomPDF\PDF as DomPDFPDF;
use Carbon\Carbon;
use DOMDocument;
use Illuminate\Http\Request;
use PDF;
use TCPDF;

class DinasLuarController extends Controller
{
    public function detail()
    {
        return view('user.dinas-luar.detail-laporan-dl');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Spt $spt)
    {
        return view('user.dinas-luar.add-laporan-dl', compact('spt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idSpt)
    {
        request()->validate([
            'daerahTujuan' => ['required'],
            'waktuPelaksanaan' => ['required'],
            'hadirDalamPertemuan' => ['required'],
            'hasilDalamPertemuan' => ['required'],
            'temuanMasalah' => ['required'],
            'saranTindak' => ['required'],
        ]);

        $spt = Spt::find($idSpt);

        $dl = LaporanDl::where('spt_id', $idSpt)->first();
        $dl->daerah_tujuan = $request->daerahTujuan;
        $dl->waktu_pelaksanaan = $request->waktuPelaksanaan;

        $kehadiran = $request->hadirDalamPertemuan;
        $dom = new \domdocument();
        $dom->loadHtml($kehadiran, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $kehadiran = $dom->saveHTML();
        $dl->kehadiran = $kehadiran;

        $hasil = $request->hasilDalamPertemuan;
        $dom = new \domdocument();
        $dom->loadHtml($hasil, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $hasil = $dom->saveHTML();
        $dl->hasil = $hasil;

        $masalah = $request->temuanMasalah;
        $dom = new \domdocument();
        $dom->loadHtml($masalah, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $masalah = $dom->saveHTML();
        $dl->masalah = $masalah;

        $saran = $request->saranTindak;
        $dom = new \domdocument();
        $dom->loadHtml($saran, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $saran = $dom->saveHTML();
        $dl->saran = $saran;

        $dl->tgl_keluar = Carbon::now();
        $dl->penanggung_jawab = $spt->pegawai->get(0)->id;
        $dl->keterangan = 1;
        $dl->save();

        return redirect()->route('spt_user');
        // dd($spt->pegawai->get(0)->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Spt $spt)
    {
        $dasar = Dasar::orderBy('nomor', 'ASC')->get();
        $peg = $spt->pegawai()->orderBy('id_golongan_pangkat', 'DESC')->get();
        return view('user.dinas-luar.detail-laporan-dl', compact('dasar', 'spt', 'peg'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Spt $spt)
    {
        $laporan_dls = LaporanDl::all();
        return view('user.dinas-luar.update-laporan-dl', compact('spt', 'laporan_dls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idSpt)
    {
        libxml_use_internal_errors(true);
        $spt = Spt::find($idSpt);

        $dl = LaporanDl::where('spt_id', $idSpt)->first();
        $dl->daerah_tujuan = $request->daerahTujuan;
        $dl->waktu_pelaksanaan = $request->waktuPelaksanaan;
        
        $hasil = $request->hasilDalamPertemuan;
        $dom = new DOMDocument();
        $dom->loadHtml(
            mb_convert_encoding($hasil, 'HTML-ENTITIES', 'UTF-8'), 
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD | LIBXML_NOEMPTYTAG
            );
        $hasil = $dom->saveXML();
        $dl->hasil = $hasil;

        $kehadiran = $request->hadirDalamPertemuan;
        $dom = new DOMDocument();
        $dom->loadHtml(
            mb_convert_encoding($kehadiran, 'HTML-ENTITIES', 'UTF-8'), 
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD | LIBXML_NOEMPTYTAG
            );
        $kehadiran = $dom->saveXML();
        // dd($kehadiran);
        $dl->kehadiran = $kehadiran;

        $masalah = $request->temuanMasalah;
        $dom = new DOMDocument();
        $dom->loadHtml(
            mb_convert_encoding($masalah, 'HTML-ENTITIES', 'UTF-8'), 
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD | LIBXML_NOEMPTYTAG
            );
        $masalah = $dom->saveXML();
        $dl->masalah = $masalah;

        $saran = $request->saranTindak;
        $dom = new DOMDocument();
        $dom->loadHtml(
            mb_convert_encoding($saran, 'HTML-ENTITIES', 'UTF-8'), 
            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD | LIBXML_NOEMPTYTAG
            );
        $saran = $dom->saveXML();
        $dl->saran = $saran;

        $dl->tgl_keluar = Carbon::now();
        $dl->penanggung_jawab = $spt->pegawai->get(0)->id;
        $dl->keterangan = 1;
        $dl->save();

        return redirect()->route('detail_laporan_dl', $idSpt);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function print(Spt $spt)
    {
        $dasar = Dasar::orderBy('nomor', 'ASC')->get();
        $peg = $spt->pegawai()->orderBy('id_golongan_pangkat', 'DESC')->get();
        // // dd($dl);
        // $pdf = PDF::loadview('user.dinas-luar.detail-laporan-dl-pdf', compact('spt', 'dasar', 'peg'))
        // ->setPaper([0,0,595.276,907.087],'potrait');

        // return $pdf->stream("dl-".$spt->nomor_surat.".pdf");

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'F4', true, 'UTF-8', false);
        $view = \View::make('user.dinas-luar.detail-laporan-dl-pdf', compact('spt', 'dasar', 'peg'));
        $html = $view->render();

        $pdf->SetTitle("dl-".$spt->nomor_surat);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output("dl-".$spt->nomor_surat.".pdf", "I");
    }
}
