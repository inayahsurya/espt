<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bidang extends Model
{
    public function pegawai(){
        return $this->hasMany('App\Pegawai', 'id_bidang');
    }

    public function kepala_bidang(){
        return $this->hasOne('App\KepalaBidang', 'id_bidang');
    }

    public function admin(){
        return $this->hasOne('App\Admin', 'id_bidang');
    }

    public function spt(){
        return $this->hasMany('App\Spt');
    }
}
