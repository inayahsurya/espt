<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spt extends Model
{  

    public function pegawai(){
        return $this->belongsToMany(Pegawai::class)->withTimestamps();
    }

    public function bidang(){
        return $this->belongsTo('App\Bidang', 'id_bidang', 'id');
    }

    public function laporanDl(){
        return $this->hasOne('App\LaporanDl');
    }

    public function arsipSpt(){
        return $this->hasMany('App\ArsipSpt', 'id', 'id_spt');
    }
}
