<?php

namespace App;

use Illuminate\Foundation\Auth\user as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function bidang(){
        return $this->belongsTo('App\Bidang', 'id_bidang');
    }
}
