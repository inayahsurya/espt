<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    public function golongan_pangkat(){
        return $this->belongsTo('App\GolonganPangkat', 'id_golongan_pangkat', 'tingkat');
    }
    public function bidang(){
        return $this->belongsTo('App\Bidang','id_bidang','id');
    }

    public function spt(){
        return $this->belongsToMany('App\Spt');
    }

    public function kepala_bidang(){
        return $this->hasOne('App\KepalaBidang', 'id', 'id_pegawai');
    }

    public function user(){
        return $this->hasOne('App\User');
    }
}
