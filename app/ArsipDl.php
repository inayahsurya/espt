<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArsipDl extends Model
{
    public function laporandl()
    {
        return $this->belongsTo('App\LaporanDl', 'id_dl');
    }
}
