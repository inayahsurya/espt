<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/logout', 'LoginController@logout')->name('logout');

Route::group(['middleware' => 'guest'], function () {
    //REGISTER
    Route::get('/register', 'RegisterController@index')->name('register');
    Route::get('/register-show', 'RegisterController@show')->name('register_show');
    Route::post('/tambah-user/{id}', 'RegisterController@store')->name('tambah_user');

    //LOGIN
    Route::get('/login', 'LoginController@getLogin')->name('login');
    Route::post('/login', 'LoginController@postLogin')->name('post_login');
});

//GROUP ADMIN
Route::group(['middleware' => 'auth:admin'], function () {
    //SPT ADMIN
    Route::get('/', 'admin\SptController@index');
    Route::get('/spt', 'admin\SptController@index')->name('spt');
    Route::get('/spt/tambah-spt-kepala-bidang', 'admin\SptController@createKabid')->name('tambah_spt_kabid');
    Route::get('/spt/tambah-spt-pegawai', 'admin\SptController@create')->name('tambah_spt');
    Route::post('/spt/tambah-spt-kepala-bidang', 'admin\SptController@store');
    Route::post('/spt/tambah-spt-pegawai', 'admin\SptController@store');
    Route::post('/spt/cetak-spt/{spt}', 'admin\SptController@print')->name('cetak_spt');
    Route::post('/spt/hapus-spt/{spt}', 'admin\SptController@destroy')->name('hapus_spt');
    Route::get('/spt/update-spt/{spt}', 'admin\SptController@edit')->name('update_spt');
    Route::post('/spt/update-spt/{spt}', 'admin\SptController@update');
    Route::get('/spt/arsip-spt/{spt}', 'admin\ArsipSptController@index')->name('arsip_spt');
    Route::post('/spt/arsip-spt/{spt}', 'admin\ArsipSptController@upload');
    Route::get('/spt/hapus-arsip/{arsip}', 'admin\ArsipSptController@destroy')->name('hapus_arsip_spt');

    //DL ADMIN
    Route::get('/laporan-dinas-luar', 'admin\DinasLuarController@index')->name('dl');
    Route::get('/laporan-dinas-luar/detail/{spt}', 'admin\DinasLuarController@show')->name('detail_dl');
    Route::get('/laporan-dinas-luar/arsip/{spt}', 'admin\ArsipDlController@index')->name('arsip_dl');
    Route::post('/laporan-dinas-luar/arsip/{spt}', 'admin\ArsipDLController@upload');
    Route::get('/laporan-dinas-luar/hapus-arsip/{arsip}', 'admin\ArsipDlController@destroy')->name('hapus_arsip_dl');
});

Route::group(['middleware' => 'auth:superadmin,admin'], function () {
    //PEGAWAI ADMIN
    Route::get('/pegawai', 'admin\PegawaiController@index')->name('pegawai');
    Route::get('/pegawai/tambah-pegawai', 'admin\PegawaiController@create')->name('tambah_pegawai');
    Route::post('/pegawai/tambah-pegawai', 'admin\PegawaiController@store');
    Route::get('/pegawai/update-pegawai/{pegawai}', 'admin\PegawaiController@edit')->name('update_pegawai');
    Route::post('/pegawai/update-pegawai/{pegawai}', 'admin\PegawaiController@update');
    Route::post('/pegawai/hapus-pegawai/{pegawai}', 'admin\PegawaiController@destroy')->name('hapus_pegawai');
});

//GROUP SUPER ADMIN
Route::group(['middleware' => 'auth:superadmin'], function () {

    //DASAR
    Route::get('/dasar', 'admin\DasarController@index')->name('dasar');
    Route::get('/dasar/tambah-dasar', 'admin\DasarController@create')->name('tambah_dasar');
    Route::post('/dasar/tambah-dasar', 'admin\DasarController@store');
    Route::post('/dasar/hapus-dasar/{dasar}', 'admin\DasarController@destroy')->name('hapus_dasar');
    Route::get('/dasar/update-dasar/{dasar}', 'admin\DasarController@edit')->name('update_dasar');
    Route::post('/dasar/update-dasar/{dasar}', 'admin\DasarController@update');

    //BIDANG
    Route::get('/bidang', 'admin\BidangController@index')->name('bidang');
    Route::get('/bidang/tambah-bidang', 'admin\BidangController@create')->name('tambah_bidang');
    Route::post('/bidang/tambah-bidang', 'admin\BidangController@store');
    Route::post('/bidang/hapus-bidang/{bidang}', 'admin\BidangController@destroy')->name('hapus_bidang');
    Route::get('/bidang/update-bidang/{bidang}', 'admin\BidangController@edit')->name('update_bidang');
    Route::post('/bidang/update-bidang/{bidang}', 'admin\BidangController@update');

    //KEPALA BIDANG
    Route::get('/kepala-bidang', 'admin\KepalaBidangController@index')->name('kabid');
    Route::get('/kepala-bidang/tambah-kepala-bidang', 'admin\KepalaBidangController@create')->name('tambah_kabid');
    Route::post('/kepala-bidang/tambah-kepala-bidang', 'admin\KepalaBidangController@store');
    Route::post('/kepala-bidang/hapus-kepala-bidang/{kabid}', 'admin\KepalaBidangController@destroy')->name('hapus_kabid');
    Route::get('/kepala-bidang/update-kepala-bidang/{kabid}', 'admin\KepalaBidangController@edit')->name('update_kabid');
    Route::post('/kepala-bidang/update-kepala-bidang/{kabid}', 'admin\KepalaBidangController@update');

    //GOLONGAN
    Route::get('/golongan', 'admin\GolonganPangkatController@index')->name('golongan');
    Route::get('/golongan/tambah-golongan', 'admin\GolonganPangkatController@create')->name('tambah_golongan');
    Route::post('/golongan/tambah-golongan', 'admin\GolonganPangkatController@store');
    Route::get('/golongan/update-golongan/{gol}', 'admin\GolonganPangkatController@edit')->name('update_golongan');
    Route::post('/golongan/update-golongan/{gol}', 'admin\GolonganPangkatController@update');
    Route::post('/golongan/hapus-golongan/{gol}', 'admin\GolonganPangkatController@destroy')->name('hapus_golongan');

    //ADMIN
    Route::get('/admin', 'admin\AdminController@index')->name('admin');
    Route::get('/admin/tambah-admin', 'admin\AdminController@create')->name('tambah_admin');
    Route::post('/admin/tambah-admin', 'admin\AdminController@store');
    Route::get('/admin/update-admin/{admin}', 'admin\AdminController@edit')->name('update_admin');
    Route::post('/admin/update-admin/{admin}', 'admin\AdminController@update')->name('update_admin');
    Route::post('/admin/hapus-admin/{admin}', 'admin\AdminController@destroy')->name('hapus_admin');
    Route::get('/admin/ganti-password/{admin}', 'admin\AdminController@editPassword')->name('update_password_admin');
    Route::post('/admin/ganti-password/{admin}', 'admin\AdminController@changePassword')->name('update_password_admin');
});

//GROUP USER
Route::group(['middleware' => 'auth:user'], function () {
    //SPT USER
    Route::get('/spt-user', 'SptUserController@index')->name('spt_user');

    //PROFILE
    Route::get('/profile/{user}', 'ProfileUserController@index')->name('profile');
    Route::get('/profile/update-profile/{user}', 'ProfileUserController@edit')->name('update_profile');
    Route::post('/profile/update-profile/{user}', 'ProfileUserController@update');
    Route::get('/profile/update-profile/update-password/{user}', 'ProfileUserController@editPassword')->name('update_password_user');
    Route::post('/profile/update-profile/update-password/{user}', 'ProfileUserController@changePassword');

    //DL USER
    Route::get('/buat-laporan-dinas-luar/{spt}', 'DinasLuarController@create')->name('tambah_laporan_dl');
    Route::post('/buat-laporan-dinas-luar/{spt}', 'DinasLuarController@store');
    Route::get('/detail-laporan-dinas-luar/{spt}', 'DinasLuarController@show')->name('detail_laporan_dl');
    Route::get('/detail-laporan-dinas-luar/update-laporan-dinas-luar/{spt}', 'DinasLuarController@edit')->name('update_laporan_dl');
    Route::post('/detail-laporan-dinas-luar/update-laporan-dinas-luar/{spt}', 'DinasLuarController@update');
});

Route::post('/cetak-laporan-dinas-luar/{spt}', 'DinasLuarController@print')->name('cetak_laporan_dl');
