<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawais', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama',255);
            $table->string('nip', 50)->unique()->nullable();
            $table->integer('status')->nullable();
            $table->integer('id_golongan_pangkat')->default(0)->nullable();
            $table->string('jabatan',255)->nullable();
            $table->unsignedBigInteger('id_bidang')->nullable();
            $table->timestamps();

            $table->foreign('id_bidang')->references('id')->on('bidangs');
            $table->foreign('id_golongan_pangkat')->references('tingkat')->on('golongan_pangkats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawais');
    }
}
