<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporanDlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_dls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('spt_id');
            $table->text('daerah_tujuan')->nullable();
            $table->text('waktu_pelaksanaan')->nullable();
            $table->longText('kehadiran')->nullable();
            $table->longText('hasil')->nullable();
            $table->longText('masalah')->nullable();
            $table->longText('saran')->nullable();
            $table->integer('penanggung_jawab')->nullable();
            $table->date('tgl_keluar')->nullable();
            $table->integer('keterangan')->default('0');
            $table->timestamps();

            $table->foreign('spt_id')->references('id')->on('spts')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_dls');
    }
}
